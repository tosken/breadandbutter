package de.leetgeeks.breadandbutter;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/**
 * Hello world!
 *
 */
public class App {

    private static final Logger log = Logger.getLogger(App.class);

    public static void main(String[] args) {
        BasicConfigurator.configure();

        log.info("Hello World!");
    }
}
